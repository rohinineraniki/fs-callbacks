const fs = require("fs");
const path = require("path");

function problem2(callback) {
  let file_path_1 = path.join(__dirname, "lipsum.txt");
  fs.readFile(file_path_1, "utf8", function (err, data) {
    if (err) {
      console.error(`Error : Reading file ${file_path_1}`, err);
    } else {
      let upper_case_data = data.toUpperCase();
      let file_path_2 = path.join(__dirname, "upper_case_data.txt");
      fs.writeFile(
        file_path_2,
        upper_case_data,
        function (err, upper_case_data) {
          if (err) {
            console.error(`Error : Writing file ${file_path_2}`, err);
          } else {
            let saved_file_path = path.join(__dirname, "filenames.txt");
            fs.writeFile(
              saved_file_path,
              file_path_2,
              function (err, appending) {
                if (err) {
                  console.error(
                    `Error : Appendind file name in ${saved_file_path}`,
                    err
                  );
                } else {
                  fs.readFile(
                    file_path_2,
                    "utf8",
                    function (err, read_uppar_case_data) {
                      if (err) {
                        console.error(
                          `Error : Reading file from ${file_path_2}`,
                          err
                        );
                      } else {
                        let lower_case_data = read_uppar_case_data
                          .toLowerCase()
                          .split(".");
                        let sentence = lower_case_data.join("\r\n");
                        let file_path_3 = path.join(__dirname, "sentence.txt");
                        fs.writeFile(
                          file_path_3,
                          sentence,
                          function (err, sentence) {
                            if (err) {
                              console.error(
                                `Error : Writing file ${file_path_3}`,
                                err
                              );
                            } else {
                              fs.appendFile(
                                saved_file_path,
                                "\r\n" + file_path_3,
                                function (err, appending) {
                                  if (err) {
                                    console.error(
                                      `Error : Appendind file name in ${saved_file_path}`,
                                      err
                                    );
                                  } else {
                                    fs.readFile(
                                      file_path_3,
                                      "utf8",
                                      function (err, read_sentence) {
                                        if (err) {
                                          console.error(
                                            `Error : Reading file from ${file_path_3}`,
                                            err
                                          );
                                        } else {
                                          let sentence_array =
                                            read_sentence.split("\n");
                                          function compareFn(name1, name2) {
                                            if (name1 > name2) {
                                              return 1;
                                            }
                                            if (name1 < name2) {
                                              return -1;
                                            } else {
                                              return 0;
                                            }
                                          }
                                          sentence_array.sort(compareFn);
                                          let file_path_4 = path.join(
                                            __dirname,
                                            "sorted_array.txt"
                                          );
                                          let sentence_string =
                                            sentence_array.join("\r\n");
                                          fs.writeFile(
                                            file_path_4,
                                            sentence_string,
                                            function (err, sorted_data) {
                                              if (err) {
                                                console.error(
                                                  `Error : Writing file ${file_path_4}`,
                                                  err
                                                );
                                              } else {
                                                fs.appendFile(
                                                  saved_file_path,
                                                  "\r\n" + file_path_4,
                                                  function (err, appending) {
                                                    if (err) {
                                                      console.error(
                                                        `Error : Appendind file name in ${saved_file_path}`,
                                                        err
                                                      );
                                                    } else {
                                                      fs.readFile(
                                                        saved_file_path,
                                                        "utf8",
                                                        function (
                                                          err,
                                                          read_all_files
                                                        ) {
                                                          if (err) {
                                                            console.error(
                                                              `Error : Reading file names ${saved_file_path}`,
                                                              err
                                                            );
                                                          } else {
                                                            let all_files_array =
                                                              read_all_files.split(
                                                                "\r\n"
                                                              );
                                                            let count_files = 0;
                                                            let deleting_each_file =
                                                              all_files_array.forEach(
                                                                (each_file) => {
                                                                  fs.unlink(
                                                                    each_file,
                                                                    function (
                                                                      err,
                                                                      file_deleted
                                                                    ) {
                                                                      if (err) {
                                                                        console.error(
                                                                          `Error : error in deleting files ${each_file},err`
                                                                        );
                                                                      } else {
                                                                        count_files += 1;

                                                                        if (
                                                                          count_files ===
                                                                          all_files_array.length
                                                                        ) {
                                                                          callback(
                                                                            null,
                                                                            "Created new files according to instructions and deleted that files."
                                                                          );
                                                                        }
                                                                      }
                                                                    }
                                                                  );
                                                                }
                                                              );
                                                          }
                                                        }
                                                      );
                                                    }
                                                  }
                                                );
                                              }
                                            }
                                          );
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    }
                  );
                }
              }
            );
          }
        }
      );
    }
  });
}

module.exports = problem2;
