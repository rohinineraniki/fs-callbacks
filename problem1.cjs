const fs = require("fs");
const path = require("path");

function problem1(callback) {
  fs.mkdir("json_files", function (err) {
    if (err) {
      console.error(err);
    } else {
      console.log("Directory created");
    }
  });
  let random_value_array = [0, 0, 0, 0, 0];
  let prev = [];
  function generate_random_value() {
    let each_random_value = Math.floor(Math.random() * 10);

    if (prev.includes(each_random_value)) {
      return generate_random_value();
    } else {
      prev.push(each_random_value);
      return each_random_value;
    }
  }

  let random_values = random_value_array.map((each) => {
    let value = generate_random_value();
    return value;
  });

  let count_file = 0;

  function delete_all_files() {
    const deleting_files = random_values.forEach((each, index) => {
      let json_path = path.join("json_files", `${each}.json`);
      fs.unlink(json_path, function (err, data) {
        if (err) {
          console.error(err);
        } else {
          count_file += 1;
          if (count_file === random_values.length) {
            callback(null, "Creating and deleting JSON files are completed");
          }
        }
      });
    });
  }
  let count_added_file = 0;
  const result = random_values.forEach((each, index) => {
    let json_path = path.join("json_files", `${each}.json`);
    fs.writeFile(json_path, " ", function (err, data) {
      if (err) {
        console.error(err);
      } else {
        count_added_file += 1;
        if (count_added_file === random_values.length) {
          delete_all_files();
        }
      }
    });
  });
}

module.exports = problem1;
