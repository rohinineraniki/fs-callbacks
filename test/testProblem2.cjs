const problem2 = require("../problem2.cjs");

const callback = (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
};

const result = problem2(callback);
