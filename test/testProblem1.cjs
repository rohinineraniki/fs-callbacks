const problem1 = require("../problem1.cjs");

const callback = (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
};

const result = problem1(callback);
